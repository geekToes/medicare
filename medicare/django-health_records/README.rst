================================================
                health_records
================================================
Medicare is a Django web app to register patient,
record important patient information, and see his
medical record, and a lot of other things which doctors 
are doing now using tons of paper.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "health_record" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'health_records',
    ]

2. Include the health_records URLconf in your project urls.py like this::

    path('/', include('health_records.urls')),

3. Run `python manage.py migrate` to create the health_records models.

4. Start the development server and visit http://127.0.0.1:8000/
   to view the web app.
