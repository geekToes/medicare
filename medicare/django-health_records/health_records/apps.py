from django.apps import AppConfig


class HealthRecordsConfig(AppConfig):
    name = 'health_records'
