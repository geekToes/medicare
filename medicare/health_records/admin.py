from django.contrib import admin
from .models import UserRecord, PersonnelRecord, Department
# Register your models here.
admin.site.register(UserRecord)
admin.site.register(PersonnelRecord)
admin.site.register(Department)