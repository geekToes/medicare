from django.views.generic import TemplateView
from django.urls import path, include
from django.contrib.auth import urls, views
from .views import SignUp, Dashboard,UserList,UserRecordList,CreateRecord, DepartmentList,PatientList, PersonnelSignUp

urlpatterns=[
path('', TemplateView.as_view(template_name='home.html'), name='home'),
path('record/patients/', CreateRecord.as_view(), name='record'),
path('signup/', SignUp.as_view(), name='signup'),
path('signup/personnel/', PersonnelSignUp.as_view(), name='personnel-signup'),
path('dashboard/', Dashboard.as_view(), name='dashboard'),
path('login/', views.LoginView.as_view(redirect_field_name="/dashboard"),name='login'),
path('logout/', views.LogoutView.as_view(), name='logout'),
path('users/', UserList.as_view(), name='users'),
path('records/', UserRecordList.as_view(), name='user-records'),
path('patients/list/', PatientList.as_view(), name='patient-list'),
path('depts/', DepartmentList.as_view(), name='depts'),
]